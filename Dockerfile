FROM python:3.7-alpine3.9

RUN apk update \
    && apk add --update build-base git make \
    && apk add tzdata \
    && cp /usr/share/zoneinfo/America/Mexico_City /etc/localtime \
    && echo "America/Mexico_City" > /etc/timezone

WORKDIR /app

COPY . /app

RUN make install-requirements

EXPOSE 8000