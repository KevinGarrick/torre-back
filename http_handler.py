import os
import requests
import logging
from libs.filter_parser import parse_params

sh = logging.StreamHandler()
logging.basicConfig(format='%(asctime)s |%(name)s|%(levelname)s|%(message)s',
                    level=logging.INFO,
                    handlers=[sh])

BIO_HOST = "https://torre.bio/api"
JOB_HOST = "https://torre.co/api"
SEARCH_HOST = "https://search.torre.co"

# - GET https://torre.bio/api/bios/$username (gets bio information of $username)
# - GET https://torre.co/api/opportunities/$id (gets job information of $id)
# - POST https://search.torre.co/opportunities/_search/?[offset=$offset&size=$size&aggregate=$aggregate] and
# - https://search.torre.co/people/_search/?[offset=$offset&size=$size&aggregate=$aggregate] (search for jobs and people in general).


class Handler(requests.Session):
    def __init__(self, api_host):
        super().__init__()
        self.api_host = api_host

    def request(self, method, url, **kwargs):
        return super().request(method, self.api_host + url, **kwargs)

    def get_by_id(self, id):
        resource_url = f"{self.base_url}/{id}"
        response = self.get(resource_url)
        if response.status_code == 200:
            return response.json()
        raise Exception(f"Resource not found: {resource_url} ({response.status_code})")

class BioHandler(Handler):
    base_url = "/bios"

class SearchHandler(Handler):
    base_url = "/_search"

    def get(self, url, query_string, params):
        response = self.post(f'{url}{self.base_url}?{query_string}', json=params)
        if response.status_code == 200:
            return response.json()
        raise Exception(f"Error getting people ({response.status_code})")

class JobHandler(Handler):
    base_url = "/opportunities"


bio_handler = BioHandler(BIO_HOST)
search_handler = SearchHandler(SEARCH_HOST)
jobs_handler = JobHandler(SEARCH_HOST)
