import os
import logging
from flask import Flask, request, jsonify
from resources import PeopleListAPI, AggregatesListAPI, JobsListAPI
from flask_cors import CORS

sh = logging.StreamHandler()
logging.basicConfig(format='%(asctime)s |%(name)s|%(levelname)s|%(message)s',
                    level=logging.INFO,
                    handlers=[sh])

app = Flask(__name__)
CORS(app)

@app.route('/', methods=['GET'])
def health():
    return jsonify(status='ok')


aggregates_list_view = AggregatesListAPI.as_view('aggregates_list_api')
app.add_url_rule('/aggregates/<entity>',
                 view_func=aggregates_list_view,
                 strict_slashes=False,
                 methods=['GET'])

people_list_view = PeopleListAPI.as_view('people_list_api')
app.add_url_rule('/people',
                 view_func=people_list_view,
                 strict_slashes=False,
                 methods=['GET'])

job_list_view = JobsListAPI.as_view('job_list_api')
app.add_url_rule('/opportunities',
                 view_func=job_list_view,
                 strict_slashes=False,
                 methods=['GET'])
