# Torre back

## How to setup
1. `pip install -r requirements.txt` 

## How to Run
1. for development
    1. `FLASK_APP=server.py python3.7 -m flask run --host=127.0.0.1 --port=8000 --reload`
2. for production
    1. `gunicorn --workers 4 --timeout 7200 --bind 127.0.0.1:8000  server:app`
