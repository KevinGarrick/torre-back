import base64
from flask import json


def parse_params(params):
    filters = {}
    pager = {}
    params_bytes = base64.b64decode(params)
    decoded_params = params_bytes.decode('utf8')
    if not decoded_params:
        return filters, pager
    parsed_params = json.loads(decoded_params)
    filters = parsed_params.get('filters', {})
    pager = build_pager(parsed_params.get('pager', {}))

    return filters, pager


def build_pager(pager):
    page = pager.get('number', 0)
    if page <= 0:
        return {}
    limit = pager.get('limit', 20)
    limit = 20 if limit == 0 else limit
    offset =  (page - 1) * limit
    pager['offset'] = offset
    pager['limit'] = limit
    pager['number'] = page
    return pager 
