from flask import Flask, request, jsonify
from flask.views import MethodView
from http_handler import search_handler, jobs_handler, bio_handler
from libs.filter_parser import parse_params
class PeopleListAPI(MethodView):
    def get(self):
        filters, pager = parse_params(request.args.get('q'))
        
        query_string = f"?page={pager.get('number', 0)}&size={pager.get('limit', 0)}&aggregate=false&offset={pager.get('offset',0)}"
        
        result = search_handler.get('/people', query_string, filters)
        return jsonify(result), 200

class JobsListAPI(MethodView):
    def get(self):
        filters, pager = parse_params(request.args.get('q'))
        
        query_string = f"?page={pager.get('number', 0)}&size={pager.get('limit', 0)}&aggregate=false&offset={pager.get('offset',0)}"
        
        result = search_handler.get('/opportunities', query_string, filters)
        return jsonify(result), 200


class AggregatesListAPI(MethodView):
    ENTITIES = ['people', 'opportunities']
    def get(self, entity):
        if entity not in self.ENTITIES:
            return jsonify(message="Entity not valid"), 400
        filters, pager = parse_params(request.args.get('q',""))
        query_string = f"?page=0&size=0&aggregate=true&offset=0"
        
        result = search_handler.get(f'/{entity}', query_string, filters)
        return jsonify(result), 200