PACKAGE_NAME=torre_backend
VERSION=0.0.1
HOST_PORT=8000
PROJECT_FOLDER=.
GIT_DIR=$(shell pwd)

clean-pyc:
	rm -Rf tests/__pycache__
	find . -name '*.pyc' -delete
	find . -name '*.pyo' -delete
	find . -name '*~' -delete

clean-build:
	rm -Rf build/
	rm -Rf dist/
	rm -Rf *.egg-info
	rm -Rf .cache/

clean: clean-pyc clean-build

build:
	docker build -t $(PACKAGE_NAME) -f Dockerfile --build-arg module_folder=$(PROJECT_FOLDER) --build-arg package_name=$(PACKAGE_NAME) $(GIT_DIR)

launch:
	FLASK_APP=server.py python3.7 -m flask run --host=0.0.0.0 --port=8000 --reload


install-requirements:
	pip -v install -r requirements.txt
